import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';


@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.css']
})
export class NotFoundComponent implements OnInit {

audio:any;
  constructor(private router:Router) { }

  ngOnInit() {
    this.audio = new Audio()
    this.audio.src="../../assets/sounds/beep.mp3"
    this.audio.load;

    this.audio.play();

  }

  returnClick(){
    this.router.navigate(['/home']);
  }
}



