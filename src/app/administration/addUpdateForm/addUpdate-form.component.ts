import {Component, Input, OnInit} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AppUser} from '../../model/appUser';
import {UsersService} from '../../shared/service/usersService';
import {passwordValidator} from '../../shared/validators/passwordValidator';
import {LoginRegisterService} from '../../shared/service/loginRegister.service';
import {CustomModalComponent} from '../../shared/custom-modal/custom-modal.component';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-update-form',
  templateUrl: './addUpdate-form.component.html',
  styleUrls: ['./addUpdate-form.component.css']
})



export class AddUpdateFormComponent implements OnInit {

  userDataFormGroup:FormGroup;
  @Input()
  userData:AppUser;


  @Input()
  registerMode:boolean=true;
  constructor( private _fb:FormBuilder,
               private _userService:UsersService,
               private _loginRegisterService:LoginRegisterService,
               private modalService: NgbModal) { }
  ngOnInit() {
    this.userDataFormGroup = this._fb.group({
      username:['',[Validators.required]],
      email:['',[Validators.email,Validators.required]],
      firstName:['',[Validators.required]],
      lastName:['',[Validators.required]],
        password:['',[Validators.required]],
        confirmPassword:['',[Validators.required,passwordValidator]],
    })

    if(this.userData){
      this.fillForm();
    }
  }

  fillForm(){
    this.userDataFormGroup.patchValue({
      username: this.userData.username,
      email: this.userData.email,
      firstName: this.userData.firstName,
      lastName: this.userData.lastName,
    })

  }
  get username():AbstractControl{
    return this.userDataFormGroup.get("username");
  }
  get email():AbstractControl{
    return this.userDataFormGroup.get("email");
  }
  get firstName():AbstractControl{
    return this.userDataFormGroup.get("firstName");
  }
  get lastName():AbstractControl{
    return this.userDataFormGroup.get("lastName");
  }

  get password(){
    return this.userDataFormGroup.get("password");
  }
  get confirmPassword(){
    return this.userDataFormGroup.get("confirmPassword");
  }



  onSubmit(e){
    e.preventDefault();

    if(!this.registerMode){
      let p:AppUser = Object.assign({},this.userData,this.userDataFormGroup.value);
      this._userService.updateProfile(p).subscribe((data)=>{
        const modalRef = this.modalService.open(CustomModalComponent);
        modalRef.componentInstance.content = "Updated profile"
        modalRef.componentInstance.title = "Information"
      },(error)=>{
        const modalRef = this.modalService.open(CustomModalComponent);
        modalRef.componentInstance.content = "Error updating profile"
        modalRef.componentInstance.title = "ERROR"
      })
    }
    else{
      let p:AppUser = new AppUser();
      p.username  = this.username.value;
      p.email =  this.email.value;
      p.firstName  =this.firstName.value;
      p.lastName  =this.lastName.value;
      p.password  =this.password.value;
      this._loginRegisterService.registerUser(p).subscribe((data)=>{
        const modalRef = this.modalService.open(CustomModalComponent);
        modalRef.componentInstance.content = "User registered"
        modalRef.componentInstance.title = "Information"
      },(error)=>{
        if(error.status === 422){
          const modalRef = this.modalService.open(CustomModalComponent);
          modalRef.componentInstance.content = "Username already Exists"
          modalRef.componentInstance.title = "ERROR"
        }
        else{
          const modalRef = this.modalService.open(CustomModalComponent);
          modalRef.componentInstance.content = "Something happened"
          modalRef.componentInstance.title = "ERROR"
        }
      })
    }



  }

}
