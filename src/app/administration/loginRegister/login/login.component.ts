import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {CurrentUserService} from '../../../shared/service/currentUserService';
import {LoginRegisterService} from '../../../shared/service/loginRegister.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AppUser} from '../../../model/appUser';
import {UserOrder} from '../../../model/userOrder';
import {ActivatedRoute, Router} from '@angular/router';
import {UsersService} from '../../../shared/service/usersService';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginFormGroup: FormGroup;

  @Output()
  userLoggedIn: EventEmitter<string> = new EventEmitter<string>();

  constructor(private active: ActivatedRoute,
              private router:Router,
              private userService:UsersService,
              private currentUserService: CurrentUserService,
              private loginRegisterService: LoginRegisterService,
              private fb: FormBuilder) {
  }
   type:string;
  ngOnInit() {
    this.loginFormGroup = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });

    this.active.params.subscribe(params => {
     this.type = params['type'];


    });
  }


  submit() {
    let username = this.loginFormGroup.controls.username.value;
    let password = this.loginFormGroup.controls.password.value;
    if (this.loginFormGroup.valid) {
      this.loginRegisterService.loginUser(username, password)
        .subscribe((data) => {

          this.currentUserService.setUserKey(data.headers.get('Authorization'));
          this.userService.getUserData(username);



          if(this.type === "returnHome"){
            this.router.navigate(['/home']);
          }
        });
    }

  }



}
