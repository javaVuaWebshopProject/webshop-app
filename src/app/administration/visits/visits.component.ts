import { Component, OnInit } from '@angular/core';
import {Visit} from '../../model/visit';
import {VisitService} from '../../shared/service/visit.service';

@Component({
  selector: 'app-visits',
  templateUrl: './visits.component.html',
  styleUrls: ['./visits.component.css']
})
export class VisitsComponent implements OnInit {

  visits:Visit[]= new Array();
  constructor(private visitService:VisitService) { }

  ngOnInit() {

    this.visitService.getVisits().subscribe((data)=>{
      this.visits = data;
    })
  }

}
