import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './loginRegister/login/login.component';
import {ReactiveFormsModule} from '@angular/forms';
import {CurrentUserService} from '../shared/service/currentUserService';
import {LoginRegisterService} from '../shared/service/loginRegister.service';
import { RegisterComponent } from './loginRegister/register/register.component';
import {AddUpdateFormComponent} from './addUpdateForm/addUpdate-form.component';
import {RoutingModule} from '../shared/routing/routing.module';
import {VisitsComponent} from './visits/visits.component';
import {VisitService} from '../shared/service/visit.service';

@NgModule({
  imports: [
    CommonModule,ReactiveFormsModule,RoutingModule
  ],
  declarations: [LoginComponent, RegisterComponent,AddUpdateFormComponent,VisitsComponent],
  providers:[CurrentUserService,LoginRegisterService,VisitService],
  exports: [LoginComponent,AddUpdateFormComponent,VisitsComponent]
})
export class AdministrationModule { }
