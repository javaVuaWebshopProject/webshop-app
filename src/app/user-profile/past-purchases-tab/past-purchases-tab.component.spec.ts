import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PastPurchasesTabComponent } from './past-purchases-tab.component';

describe('PastPurchasesTabComponent', () => {
  let component: PastPurchasesTabComponent;
  let fixture: ComponentFixture<PastPurchasesTabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PastPurchasesTabComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PastPurchasesTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
