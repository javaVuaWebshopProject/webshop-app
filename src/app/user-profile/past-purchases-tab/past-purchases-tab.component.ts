import { Component, OnInit } from '@angular/core';
import {CurrentUserService} from '../../shared/service/currentUserService';
import {UserOrder} from '../../model/userOrder';
import {OrderService} from '../../shared/service/orderService';
import {FormBuilder, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-past-purchases-tab',
  templateUrl: './past-purchases-tab.component.html',
  styleUrls: ['./past-purchases-tab.component.css']
})
export class PastPurchasesTabComponent implements OnInit {

  orders:UserOrder[];
   currentItemCount: number = 0;
  private currentPage: number;
  isAdmin = false;
  constructor(private currentUserService:CurrentUserService,
              private orderService:OrderService,
              private  formBuilder:FormBuilder) { }

  ngOnInit() {
 this.loadData();
 this.isAdmin= this.currentUserService.isUserAdmin();
  }

  formgroup: FormGroup = this.formBuilder.group({
    search: ''
  });
  private loadAllPastPurchases(pageNumber:number,orderBy:string,asc:boolean) {
    this.orderService.getAllOrders(pageNumber,orderBy,asc)
      .subscribe((data)=>{

        this.orders = data;
      })

    this.orderService.getAllOrderCount()
      .subscribe((data)=>{
        this.currentItemCount = data;
      })
  }

  pageChanged(event){

    this.currentPage = event;
    this.loadData();
  }
  private loadPastPurchases(username: string,pageNumber:number,orderBy:string,asc:boolean) {

    this.orderService.getOrdersByUser(username,pageNumber,orderBy,asc)
      .subscribe((data)=>{

        this.orders = data;
      })

    this.orderService.getOrderCountByUserID(this.currentUserService.getRegisteredUser().idUser)
      .subscribe((data)=>{
        this.currentItemCount = data;
      })

  }

  filterClick(){

   let value = this.formgroup.controls.search.value;
    if(value === ""){
      this.loadData();
    }
    else{
      this.loadPastPurchases(value,0,'dateOfCreation',true);
    }
  }
  private loadData(pageNumber:number= 0,orderBy:string = 'dateOfCreation',asc:boolean = true) {
    if(this.currentUserService.isUserAdmin()){
      this.loadAllPastPurchases(pageNumber,orderBy,asc);
    }
    else{
      this.loadPastPurchases(this.currentUserService.getRegisteredUser().username,
                              pageNumber,orderBy,asc);
    }
  }
}
