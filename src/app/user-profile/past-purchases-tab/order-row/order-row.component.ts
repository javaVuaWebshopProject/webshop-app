import {Component, Input, OnInit} from '@angular/core';
import {UserOrder} from '../../../model/userOrder';

@Component({
  selector: 'app-order-row',
  templateUrl: './order-row.component.html',
  styleUrls: ['./order-row.component.css']
})
export class OrderRowComponent implements OnInit {

  @Input()
  order:UserOrder
  constructor() { }

  ngOnInit() {



  }

}
