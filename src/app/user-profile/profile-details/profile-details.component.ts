import { Component, OnInit } from '@angular/core';
import {AppUser} from '../../model/appUser';
import {CurrentUserService} from '../../shared/service/currentUserService';

@Component({
  selector: 'app-profile-details',
  templateUrl: './profile-details.component.html',
  styleUrls: ['./profile-details.component.css']
})
export class ProfileDetailsComponent implements OnInit {

  isEdit:boolean = false;
  userProfile:AppUser;
  constructor(private userDetailsService:CurrentUserService) { }

  ngOnInit() {
    this.userProfile = this.userDetailsService.getRegisteredUser();


  }

}
