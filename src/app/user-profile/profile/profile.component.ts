import { Component, OnInit } from '@angular/core';
import {UsersService} from '../../shared/service/usersService';
import {CurrentUserService} from '../../shared/service/currentUserService';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

isAdmin:boolean = false;
  constructor(private userService:CurrentUserService) { }

  ngOnInit() {
    this.isAdmin = this.userService.isUserAdmin();
  }


}
