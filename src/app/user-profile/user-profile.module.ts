import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileComponent } from './profile/profile.component';
import { ProfileDetailsComponent } from './profile-details/profile-details.component';
import { PastPurchasesTabComponent } from './past-purchases-tab/past-purchases-tab.component';
import { OrderRowComponent } from './past-purchases-tab/order-row/order-row.component';
import {CartModule} from '../cart/cart.module';
import { AddUpdateFormComponent } from '../administration/addUpdateForm/addUpdate-form.component';
import {ReactiveFormsModule} from '@angular/forms';
import {AdministrationModule} from '../administration/administration.module';
import {SharedModule} from '../shared/shared.module';


@NgModule({
  imports: [
    CommonModule,
    CartModule,
    ReactiveFormsModule,
    AdministrationModule,
    SharedModule
  ],
  declarations: [ProfileComponent, ProfileDetailsComponent, PastPurchasesTabComponent, OrderRowComponent]
})
export class UserProfileModule { }
