import { Component, OnInit } from '@angular/core';
import {Product} from '../../model/product';
import {ActivatedRoute} from '@angular/router';
import {ProductService} from '../../shared/service/product.service';


@Component({
  selector: 'app-productlist',
  templateUrl: './productlist.component.html',
  styleUrls: ['./productlist.component.css']
})
export class ProductlistComponent implements OnInit {


  products:Product[];
  totalProductCount = 0;
  searchParam:string;
  subCategoryID:number;
  currentPage:number = 0;

  currentSort:string = 'price';
  currentAsc:boolean = true;
  constructor(private active:ActivatedRoute,private productService:ProductService) { }

  ngOnInit() {

    this.active.params.subscribe(params =>{

      this.searchParam = params['searchQuery'];
      this.subCategoryID = params['categoryID'];
      this.getProducts();

    })

  }

  private getProducts(pageNumber:number= 0,orderBy:string = 'price',asc:boolean = true) {
    if(this.searchParam != null){
      this.productService.getProductsbySearch(this.searchParam,pageNumber,orderBy,asc)
        .subscribe((data) =>{
          this.products = data;
        })

      this.productService.getProductCountByKeyword(this.searchParam)
        .subscribe((data)=>{
          this.totalProductCount = data;
        })
    }
    if(this.subCategoryID !=null){
      this.productService.getProductBySubCategory(this.subCategoryID,pageNumber,orderBy,asc)
        .subscribe((data) =>{
          this.products = data;
        })


      this.productService.getProductCountBySubCategory(this.subCategoryID)
        .subscribe((data)=>{
          this.totalProductCount = data;
        })
    }

  }

  setSort(orderby:string,ascending:boolean){
    this.currentSort = orderby;
    this.currentAsc = ascending;
    this.getProducts(this.currentPage,orderby,ascending);
  }

  pageChanged(page:number){
    this.currentPage = page;
    this.getProducts(page,this.currentSort,this.currentAsc);

  }

}
