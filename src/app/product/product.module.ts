import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductlistComponent } from './productlist/productlist.component';
import { ProductDetailComponent } from './product-detail/product-detail.component';
import { ProductSpecificationComponent } from './product-detail/product-specification/product-specification.component';
import {ProductService} from '../shared/service/product.service';
import {SharedModule} from '../shared/shared.module';
import {ShoppingCartService} from '../shared/service/shoppingCartService';

@NgModule({
  imports: [
    CommonModule,
    SharedModule
  ],
  providers:[ProductService,ShoppingCartService],
  declarations: [ProductlistComponent, ProductDetailComponent, ProductSpecificationComponent]
})
export class ProductModule { }
