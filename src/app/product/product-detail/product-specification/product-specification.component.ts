import {Component, Input, OnInit} from '@angular/core';
import {Specification} from '../../../model/specification';
import {Link} from '../../../model/link';

@Component({
  selector: 'app-product-specification',
  templateUrl: './product-specification.component.html',
  styleUrls: ['./product-specification.component.css']
})
export class ProductSpecificationComponent implements OnInit {

  @Input()
  specifications:Specification[];

  @Input()
  links:Link[];
  constructor() { }

  ngOnInit() {
  }

}
