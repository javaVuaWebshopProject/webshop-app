import {Component, Input, OnInit} from '@angular/core';
import {Product} from '../../model/product';
import {ProductService} from '../../shared/service/product.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Item} from '../../model/item';
import {ShoppingCartService} from '../../shared/service/shoppingCartService';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {


  @Input()
  product:Product;
  @Input()
  compact:boolean = false;
  newPrice:number;
  quantityLocal:number = 1
  imageData:string

  constructor(private active:ActivatedRoute,
              private productService:ProductService,
              private cartService:ShoppingCartService,
              private router:Router) { }

  ngOnInit() {
    if(!this.product){
      this.active.params.subscribe(params =>{
        let idProduct:number = params['id'];
        this.getProduct(idProduct);


      })
    }else{
      this.newPrice = this.product.price;
      this.getImage();
    }

  }

  productClick(){
    this.router.navigate(["product/",this.product.idProduct]);
  }

  private getProduct(idProduct:number) {
    this.productService.getProductByID(idProduct)
      .subscribe((data) =>{
        this.product = data;
        this.newPrice = data.price;
        this.getImage();
      })
  }


  changePrice(quantity:number){
    this.quantityLocal = quantity;
    this.newPrice = this.product.price * quantity

  }

  addToCart(){
    let i = new Item();
    i.product = this.product
    i.quantity = this.quantityLocal;
    i.totalItemPrice = this.newPrice;

    this.cartService.put(i);

  }

  private getImage() {
    let filename = this.product.fileName;

    this.productService.getImageForProduct(filename)
      .subscribe((data)=>{
        this.imageData = data
      },(data)=>{

        this.imageData = data.error.text
      })
  }
}
