import {AfterViewChecked, Component, OnInit, ViewContainerRef} from '@angular/core';
import {ShoppingCartService} from '../../shared/service/shoppingCartService';
import {CurrentUserService} from '../../shared/service/currentUserService';
import {Address} from '../../model/address';
import {UserOrder} from '../../model/userOrder';
import {OrderService} from '../../shared/service/orderService';
import {Item} from '../../model/item';
import {ItemService} from '../../shared/service/itemService';
import {UsersService} from '../../shared/service/usersService';
import {NgbModal, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {CustomModalComponent} from '../../shared/custom-modal/custom-modal.component';

declare let paypal:any;
@Component({
  selector: 'app-cart-homepage',
  templateUrl: './cart-homepage.component.html',
  styleUrls: ['./cart-homepage.component.css']
})
export class CartHomepageComponent implements OnInit,AfterViewChecked {

userOrder:UserOrder;
  isAddressSelected:boolean = false;
  paypalItems:any[] = new Array();
  canLoad:boolean =false;
  scriptTagElement:any;
  constructor(public cartService:ShoppingCartService,
              public currentUserSerice:CurrentUserService,
              private orderService:OrderService,
              private itemService:ItemService,
              private userService:UsersService,
              private modalService: NgbModal,
              private viewRef: ViewContainerRef) {
    this.userOrder  = new UserOrder();
  }

  ngOnInit() {
    this.cartService.refresh();
    this.copyPaypal();
  }
  addressPosted(e){
    this.userService.getUserData(this.currentUserSerice.getRegisteredUser().username);
  }

  userLoggedIn(event){
}

  placeOrder(){
    this.userOrder.user = this.currentUserSerice.getRegisteredUser();
    this.userOrder.totalPrice = this.cartService.shoppingCart.total;
    this.userOrder.dateOfCreation = new Date();
   this.orderService.placeOrder(this.userOrder)
     .subscribe((data)=>{
       this.orderItems(data)
     })
  }

  selectedAddress(orderTemp:UserOrder){
    this.isAddressSelected = true;
    this.userOrder.address = orderTemp.address;

    if(this.cartService.shoppingCart.itemsInCart.length == 0){
      this.canLoad = false;
    }else{
      this.canLoad = true;
    }
  }

  private orderItems(data: number) {
    this.userOrder.idOrder = data;
    for(let item of this.cartService.shoppingCart.itemsInCart){
      item.userOrder = this.userOrder;

    }
    this.itemService.orderItems(this.cartService.shoppingCart.itemsInCart)
      .subscribe((data)=>{
        this.cartService.clear();
        const modalRef = this.modalService.open(CustomModalComponent);
        modalRef.componentInstance.content = "Order placed"
        modalRef.componentInstance.title = "Information"
      })
  }
  quantityChanged(){
    this.cartService.refresh();

   this.copyPaypal();
   this.initPayPal();

  }
  itemRemoved(){
    this.cartService.refresh();

    this.copyPaypal();
    this.initPayPal();
  }


  addScript: boolean = false;

  paypalConfig = {
    env:"sandbox",
    client:{
      sandbox:'AX2eDSFWqQgGeIZQqz-9WEzjdfKnFQH6G3dU1DxDLxvIpzTJH-qfRoZoBqyLQ3jN_qsLcmOdo8K_NobB',
      production:''
    },
    commit: true,
    payment:(data,actions) =>{
      return actions.payment.create({
        payment:{
          transactions:[
            {
              amount: {total:this.cartService.shoppingCart.total,currency:'USD'},
              description:"Pay for this",
              item_list:
                {
                  items: this.paypalItems
              }
            }
          ]
        }
      })
    },
    onAuthorize: (data,actions) =>{
      return actions.payment.execute().then((payment) =>{
        this.userOrder.paypalPaymentId = payment.id;
        this.placeOrder();
      })
    }
  };

  ngAfterViewChecked(): void {
   this.initPayPal();
  }
  private initPayPal() {
    if(!this.addScript && this.canLoad){
      this.addPaypalScript().then(()=>{
        paypal.Button.render(this.paypalConfig,'#paypal-button')
      })
    }
  }

  private addPaypalScript() {
    this.addScript = true;
    if(this.scriptTagElement){
      document.body.removeChild(this.scriptTagElement);
      }
    return  new Promise((resolve,reject) =>{
      let scriptTagElement  = document.createElement('script');
      scriptTagElement.src = 'https://www.paypalobjects.com/api/checkout.js';
      scriptTagElement.onload = resolve;
      document.body.appendChild(scriptTagElement);
    })
  }

  private copyPaypal() {
    this.paypalItems = new Array();
    let items = this.cartService.shoppingCart.itemsInCart;
    items.forEach((item)=>{
      let paypalItem:any = {};
      paypalItem.name = item.product.name;
      paypalItem.description = item.product.description;
      paypalItem.price = item.product.price
      paypalItem.currency="USD"
      paypalItem.tax = "1.00"
      paypalItem.quantity = item.quantity;
      this.paypalItems.push(paypalItem);
    })
  }


}
