import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CartHomepageComponent } from './cart-homepage.component';

describe('CartHomepageComponent', () => {
  let component: CartHomepageComponent;
  let fixture: ComponentFixture<CartHomepageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CartHomepageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CartHomepageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
