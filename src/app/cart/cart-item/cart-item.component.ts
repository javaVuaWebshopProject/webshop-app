import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Item} from '../../model/item';
import {ShoppingCartService} from '../../shared/service/shoppingCartService';

@Component({
  selector: 'app-cart-item',
  templateUrl: './cart-item.component.html',
  styleUrls: ['./cart-item.component.css']
})
export class CartItemComponent implements OnInit {

  @Input()
  item:Item;

  @Input()
  showStepper:boolean;
  @Output()
  quantityChanged:EventEmitter<boolean> = new EventEmitter<boolean>();

  @Output()
  itemRemoved:EventEmitter<boolean> = new EventEmitter<boolean>();


  constructor(private cartService:ShoppingCartService) { }

  ngOnInit() {

  }

  removeItem(){
    this.cartService.remove(this.item);
    this.itemRemoved.emit(true);
  }

  priceChanged(quantity:number){
    this.item.quantity = quantity
    this.quantityChanged.emit(true);
  }

}
