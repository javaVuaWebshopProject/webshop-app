import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CartHomepageComponent } from './cart-homepage/cart-homepage.component';
import { CartItemComponent } from './cart-item/cart-item.component';
import {SharedModule} from '../shared/shared.module';
import {AdministrationModule} from '../administration/administration.module';
import { ShippingInfoComponent } from './shipping-info/shipping-info.component';
import {ReactiveFormsModule} from '@angular/forms';
import {OrderService} from '../shared/service/orderService';
import {ItemService} from '../shared/service/itemService';
import {RouterModule} from '@angular/router';
import { FormAddUpdateComponent } from './shipping-info/form-add-update/form-add-update.component';
import {AddressService} from '../shared/service/addressService';
import {CustomModalComponent} from '../shared/custom-modal/custom-modal.component';
import {ComponentService} from '../shared/service/componentService';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    AdministrationModule,
    ReactiveFormsModule,RouterModule

  ],
  entryComponents:[CustomModalComponent],
  declarations: [CartHomepageComponent, CartItemComponent, ShippingInfoComponent, FormAddUpdateComponent],
  providers:[AddressService,ComponentService],
  exports:[CartItemComponent]
})
export class CartModule { }
