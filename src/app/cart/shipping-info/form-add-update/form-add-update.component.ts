import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AddressService} from '../../../shared/service/addressService';
import {Address} from '../../../model/address';
import {CurrentUserService} from '../../../shared/service/currentUserService';


@Component({
  selector: 'app-form-add-update',
  templateUrl: './form-add-update.component.html',
  styleUrls: ['./form-add-update.component.css']
})
export class FormAddUpdateComponent implements OnInit {

  shippingInfoFormGroup:FormGroup;

  @Output()
  addressPosted:EventEmitter<boolean> =new EventEmitter<boolean>();
  constructor(private fb:FormBuilder,
              private addressService:AddressService,
              private currentUserService:CurrentUserService) {
  }

  ngOnInit() {
    this.shippingInfoFormGroup=this.fb.group({
      addressName:['',Validators.required],
      postalCode:['',Validators.required]
    })
  }


  submitForm(){
    if(this.shippingInfoFormGroup.valid){
      let p:Address = Object.assign({},this.shippingInfoFormGroup.value);

      p.user = this.currentUserService.getRegisteredUser();

      this.addressService.insertAddress(p)
        .subscribe((success)=>{
          this.addressPosted.emit(true);
        })
    }
  }



}
