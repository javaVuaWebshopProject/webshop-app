import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AppUser} from '../../model/appUser';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Address} from '../../model/address';
import {UserOrder} from '../../model/userOrder';

@Component({
  selector: 'app-shipping-info',
  templateUrl: './shipping-info.component.html',
  styleUrls: ['./shipping-info.component.css']
})
export class ShippingInfoComponent implements OnInit {

  @Input()
  user:AppUser;

  shippingInfoFormGroup:FormGroup;

  @Output()
  addressSelected:EventEmitter<UserOrder> =new EventEmitter<UserOrder>();
  constructor(private fb:FormBuilder) {
  }

  ngOnInit() {
    this.shippingInfoFormGroup=this.fb.group({
      shipping:['',Validators.required],
    })

  }



  changeOrder(){
    if(this.shippingInfoFormGroup.valid){
      var id = this.shippingInfoFormGroup.controls['shipping'].value;

      var address = this.user.addresses.findIndex(a=>a.iDAddress == id);

      let userOder:UserOrder = new UserOrder();
      userOder.address = this.user.addresses[address]


      this.addressSelected.emit(userOder);
    }

  }

}
