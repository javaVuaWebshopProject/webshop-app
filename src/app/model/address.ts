import {AppUser} from './appUser';

export interface Address{
  iDAddress:number,
  addressName:string,
  postalCode:number,
  user:AppUser
}
