import {Product} from './product';

export interface Specification{
  idSpecification:number,
  type:string,
  value:string,
  product:Product
}
