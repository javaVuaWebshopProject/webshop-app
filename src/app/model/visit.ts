import {AppUser} from './appUser';

export interface Visit {
  idVisit:number,
  dateOfLogin:Date,
  ipAddress:string,
  user:AppUser
}
