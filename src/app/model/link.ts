import {Product} from './product';

export interface Link{
  idLink:number,
  name:string,
  value:string,
  product:Product
}
