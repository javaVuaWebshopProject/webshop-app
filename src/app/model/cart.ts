import {Item} from './item';

export interface Cart {
  put(item: Item);

  remove(item: Item);

  removeQuantity(item: Item, quantity: number);


}
