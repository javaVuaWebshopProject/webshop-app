import {AppUser} from './appUser';
import {Address} from './address';
import {Item} from './item';

export  class UserOrder{
  idOrder:number;
  dateOfCreation:Date;
  totalPrice:number;
  address:Address;
  user:AppUser;
  paypalPaymentId:string;
  items:Item[]
}
