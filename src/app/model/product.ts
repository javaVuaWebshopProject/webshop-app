import {SubCategory} from './subCategory';
import {Specification} from './specification';
import {Link} from './link';

export interface Product{
  idProduct:number,
  name:string,
  price:number,
  description:string,
  category:SubCategory,
  specifications:Specification[],
  links:Link[],
  fileName:string;
}
