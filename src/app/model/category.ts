import {SubCategory} from './subCategory';

export interface Category{
  idCategory:number,
  name:string
  subCategories:SubCategory[]
}
