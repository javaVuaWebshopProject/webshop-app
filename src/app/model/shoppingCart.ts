import {Item} from './item';
import {Cart} from './cart';

export class ShoppingCart implements Cart {
  itemsInCart: Item[];
  total: number;


  constructor() {
    this.itemsInCart = new Array();
    this.total = 0;

      if(sessionStorage.getItem("items") != null){
        this.itemsInCart =JSON.parse( sessionStorage.getItem("items"));

        this.calculateTotalItemPrice()
        this.calculateTotal()
      }
  }

  put(item: Item) {
    this.itemsInCart.push(item);
    this.calculateTotal();
    this.saveItems();
  }


  remove(item: Item) {
    let index = this.itemsInCart.findIndex(si => si == item);
    this.itemsInCart.splice(index, 1);
    this.calculateTotal();
    this.saveItems();
  }

  public removeQuantity(item: Item, quantity: number) {
    let index = this.itemsInCart.findIndex(si => si == item);
    let foundItem = this.itemsInCart[index];

    foundItem.quantity -=quantity;
    this.calculateTotalItemPrice();
  }

  private calculateTotalItemPrice(){
    for (let item of this.itemsInCart) {
      item.totalItemPrice = item.product.price * item.quantity;
    }
  }
  private calculateTotal() {

    this.total = 0;
    for (let item of this.itemsInCart) {
      this.total += item.totalItemPrice;
    }
  }

  public getItemQuantity() {
    return this.itemsInCart.length;
  }


  private saveItems() {
    sessionStorage.setItem("items",JSON.stringify(this.itemsInCart));
  }

  refresh(){
    this.calculateTotalItemPrice()
    this.calculateTotal()
  }

  clear(){
    this.itemsInCart = []
    this.total = 0;
    this.saveItems();
  }
}
