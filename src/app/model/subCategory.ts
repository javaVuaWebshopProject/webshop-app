import {Category} from './category';

export  interface SubCategory{
  idSubcategory:number,
  name:string,
  category:Category
}
