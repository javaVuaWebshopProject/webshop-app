import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup} from '@angular/forms';
import {ShoppingCartService} from '../shared/service/shoppingCartService';
import {CurrentUserService} from '../shared/service/currentUserService';
import {ProductService} from '../shared/service/product.service';
import {Category} from '../model/category';
import {SubCategory} from '../model/subCategory';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  showSubcategories: boolean;

  categories: Category[];
  is404:boolean=false

  subcategories:SubCategory[];

  constructor(private router: Router
    , private formBuilder: FormBuilder,
              public  cartService: ShoppingCartService,
              public currentUserService: CurrentUserService,
              private productService: ProductService) {
  }

  formgroup: FormGroup = this.formBuilder.group({
    search: ''
  });

  ngOnInit() {
    this.router.events.subscribe((val)=>{
      this.is404 = this.router.url ==='/notFound';
    })

    this.productService.getCategories()
      .subscribe((data) => {
        this.categories = data;
      });
  }


  searchClick() {

    this.router.navigate(['/searchResults', this.formgroup.controls.search.value]);
  }
  logout(){
    this.currentUserService.logout();
  }

  showSubCategories(category:Category){
    this.showSubcategories = false;
    this.subcategories = category.subCategories;
  }

  leftArea(){
    if(!this.showSubcategories){
      this.subcategories = null;
    }
  }

  clicked(subCategory:SubCategory){
    this.router.navigate(['/searchResults/category', subCategory.idSubcategory]);
  }

  categoryClicked() {
    this.showSubcategories = true;
  }
}
