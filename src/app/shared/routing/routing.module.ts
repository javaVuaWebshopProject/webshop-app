import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HomepageComponent} from '../../home/homepage/homepage.component';
import {RouterModule, Routes} from '@angular/router';
import {ProductlistComponent} from '../../product/productlist/productlist.component';
import {ProductDetailComponent} from '../../product/product-detail/product-detail.component';
import {ProfileComponent} from '../../user-profile/profile/profile.component';
import {CartHomepageComponent} from '../../cart/cart-homepage/cart-homepage.component';
import {LoginComponent} from '../../administration/loginRegister/login/login.component';
import {RegisterComponent} from '../../administration/loginRegister/register/register.component';
import {NotFoundComponent} from '../../not-found/not-found.component'

const appRoutes: Routes = [
  { path: 'home', component: HomepageComponent },
  {path:'login/:type',component:LoginComponent},
  {path:'register',component:RegisterComponent},
  { path: 'searchResults/:searchQuery', component: ProductlistComponent },
  { path: 'searchResults/category/:categoryID', component: ProductlistComponent },
  { path: 'product/:id', component: ProductDetailComponent },
   {path:'cart',component:CartHomepageComponent},
  { path: 'profile', component: ProfileComponent },
  { path: "", component: HomepageComponent},
  {path:"notFound",component: NotFoundComponent},
  {path: '**', redirectTo: '/notFound'}

];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(appRoutes)
  ],
  declarations: [],
  exports:[RouterModule]
})
export class RoutingModule { }
