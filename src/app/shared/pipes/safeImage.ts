import {DomSanitizer} from '@angular/platform-browser';
import {Pipe} from '@angular/core';

@Pipe({name: "safeImage"})
export  class SafeImage {

  constructor(private sanitizer:DomSanitizer){}
  transform(imageData){
    return this.sanitizer.bypassSecurityTrustResourceUrl(imageData);
  }
}
