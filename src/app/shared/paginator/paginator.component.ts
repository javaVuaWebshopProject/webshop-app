import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ProductService} from '../service/product.service';

@Component({
  selector: 'app-paginator',
  templateUrl: './paginator.component.html',
  styleUrls: ['./paginator.component.css']
})
export class PaginatorComponent implements OnInit {

  constructor(private productService:ProductService){

  }
  @Input()
  itemCount:number

  @Output()
  pageChanged:EventEmitter<number> = new EventEmitter<number>();

  pages:number[];
  productsPerPage:number = 10;

  currentPage:number = 0;
  ngOnInit() {
    this.paginatorItems();
  }

  private calculatePagination(){
    let pagesCount = this.itemCount / this.productsPerPage;
    this.pages = Array.from(new Array(Math.ceil(pagesCount)),(val,index)=>index);
  }


  pageClick(currentElementIndex:number){
    if(currentElementIndex >= 0 && currentElementIndex <this.pages.length){
      this.currentPage = currentElementIndex;
      this.pageChanged.emit(currentElementIndex);
    }
  }

  private paginatorItems() {
    this.calculatePagination()
  }
}
