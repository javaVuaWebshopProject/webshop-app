import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-stepper-component',
  templateUrl: './stepper-component.component.html',
  styleUrls: ['./stepper-component.component.css']
})
export class StepperComponentComponent implements OnInit {

  @Input()
  number:number = 1;

  @Output()
  buttomChange:EventEmitter<number> = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }

  increaseNumber(){
    this.number++;
    this.buttomChange.emit(this.number);
  }

  decreaseNumber(){
    this.number--;
    this.buttomChange.emit(this.number);
  }

  textChanged(event){

    this.buttomChange.emit(this.number);
  }

}
