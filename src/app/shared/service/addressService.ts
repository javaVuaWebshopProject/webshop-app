import {Injectable} from '@angular/core';
import {UserOrder} from '../../model/userOrder';
import {Observable} from 'rxjs/Observable';
import {ApiUrlService} from './apiurl.service';
import {HttpClient} from '@angular/common/http';
import {Address} from '../../model/address';
import {CurrentUserService} from './currentUserService';
import {connectableObservableDescriptor} from 'rxjs/observable/ConnectableObservable';
import {addRemoveViewFromContainer} from '@angular/core/src/render3/node_manipulation';

@Injectable()
export  class AddressService {


  constructor(private _apiUrlService:ApiUrlService,
              private currentUserService:CurrentUserService,
              private http:HttpClient) {
  }

  insertAddress(address:Address):Observable<boolean>{
    let headers = this.currentUserService.getAuthHeader();

    return this.http.post<boolean>(this._apiUrlService.address,address,{headers:headers})
  }

}
