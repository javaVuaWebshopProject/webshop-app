import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {CurrentUserService} from './currentUserService';
import {ApiUrlService} from './apiurl.service';
import {UserOrder} from '../../model/userOrder';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class OrderService {


  constructor(private http: HttpClient,
              private currentUserService: CurrentUserService,
              private apiUrlService: ApiUrlService) {
  }


  placeOrder(order: UserOrder):Observable<number> {

    let headers = this.currentUserService.getAuthHeader();

    return this.http.post<number>(this.apiUrlService.order+"place",order,{headers:headers});
  }
  getOrdersByUser(username:string,pageNumber:number,orderBy:string,asc:boolean):Observable<UserOrder[]>{
    let headers = this.currentUserService.getAuthHeader();

    return this.http.get<UserOrder[]>(this.apiUrlService.order+"getOrders/"+username+"/"+pageNumber
      +"/"+orderBy+"/"+asc,{headers:headers})
  }


  getAllOrders(pageNumber:number,orderBy:string,asc:boolean):Observable<UserOrder[]>{
    let headers = this.currentUserService.getAuthHeader();

    return this.http.get<UserOrder[]>(this.apiUrlService.order+"getAll/"+pageNumber
      +"/"+orderBy+"/"+asc,{headers:headers})
  }

  getAllOrderCount():Observable<number>{
    let headers = this.currentUserService.getAuthHeader();

    return this.http.get<number>(this.apiUrlService.order+"count/",{headers:headers})
  }

  getOrderCountByUserID(userID:number):Observable<number>{
    let headers = this.currentUserService.getAuthHeader();

    return this.http.get<number>(this.apiUrlService.order+"count/"+userID,{headers:headers})
  }
}
