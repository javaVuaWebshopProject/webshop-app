import {Observable} from 'rxjs/Observable';
import {Product} from '../../model/product';
import {Category} from '../../model/category';
import {ApiUrlService} from './apiurl.service';
import {HttpClient} from '@angular/common/http';
import {Visit} from '../../model/visit';
import {Injectable} from '@angular/core';
import {CurrentUserService} from './currentUserService';

@Injectable()
export class VisitService {

  constructor(private http:HttpClient,private apiUrlService:ApiUrlService,private currentUserSerivice:CurrentUserService) {
  }

  public getVisits():Observable<Visit[]>{

    let headers = this.currentUserSerivice.getAuthHeader();
    return this.http.get<Visit[]>(this.apiUrlService.visit,{headers:headers});
  }


  recordVisit(visit:Visit){
    let headers = this.currentUserSerivice.getAuthHeader();
  return this.http.post<boolean>(this.apiUrlService.visit,visit,{headers:headers});
  }
}
