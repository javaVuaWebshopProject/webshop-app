import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Product} from '../../model/product';
import {AppUser} from '../../model/appUser';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {ApiUrlService} from './apiurl.service';
import {CurrentUserService} from './currentUserService';
import {VisitService} from './visit.service';
import {Visit} from '../../model/visit';

@Injectable()
export class UsersService {

  constructor(private _http:HttpClient,
              private _apiUrlService:ApiUrlService,
              private userService:CurrentUserService,
              private visitService:VisitService){}
  public updateProfile(user:AppUser):Observable<number>{
    let header = this.userService.getAuthHeader();
    return this._http.put<number>(this._apiUrlService.users+"update",user,{headers:header});
  }


  getUserData(username:string){
    let headers = this.userService.getAuthHeader();
    let response = this._http.get<AppUser>(this._apiUrlService.users+"getInfo/" + username, {headers: headers})
      .catch(this.handleError);


    response.subscribe((data)=>{
      this.userService.setUser(data);

      this._http.get('https://api.ipify.org?format=json').subscribe(data => {
        let visit:Visit = {
          dateOfLogin: new Date(),
          ipAddress: data['ip'],
          user: this.userService.getRegisteredUser(),
          idVisit:0
        }
        this.visitService.recordVisit(visit).subscribe((data)=>{})
      });
    })
  }

  private handleError(err: HttpErrorResponse) {
    return Observable.throw(err);
  }

}
