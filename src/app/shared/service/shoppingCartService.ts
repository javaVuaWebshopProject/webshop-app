import {ShoppingCart} from '../../model/shoppingCart';
import {Cart} from '../../model/cart';
import {Item} from '../../model/item';
import {Injectable} from '@angular/core';

@Injectable()
export class ShoppingCartService implements Cart{

  shoppingCart:ShoppingCart;
  quantity:number = 0;
  constructor() {
       this.shoppingCart = new ShoppingCart();
       this.quantity = this.shoppingCart.getItemQuantity();
  }

  put(item: Item) {
    item.totalItemPrice = item.product.price * item.quantity;
    this.shoppingCart.put(item)
    this.quantity++;

  }

  remove(item: Item) {
    this.shoppingCart.remove(item)
    item.totalItemPrice = item.product.price * item.quantity;

  }

  removeQuantity(item: Item, quantity: number) {
    this.shoppingCart.removeQuantity(item,quantity)

  }

  refresh(){
    this.shoppingCart.refresh();
  }

  clear(){
    this.shoppingCart.clear();
  }








}
