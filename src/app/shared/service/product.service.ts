import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Product} from '../../model/product';
import {Observable} from 'rxjs/Observable';
import {Category} from '../../model/category';
import {ApiUrlService} from './apiurl.service';


@Injectable()
export class ProductService{



  constructor(private http:HttpClient,private apiUrlService:ApiUrlService) {
  }


  public getProductsbySearch(searchparam:string,pageNumber:number,orderBy:string,asc:boolean):Observable<Product[]>{

    return this.http.get<Product[]>(this.apiUrlService.product + "query/" + searchparam+
      "/"+pageNumber +"/"+orderBy+"/"+asc);
  }

  public getProductByID(id:number):Observable<Product>{

    return this.http.get<Product>(this.apiUrlService.product  + id);
  }

  public getCategories():Observable<Category[]>{

    return this.http.get<Category[]>(this.apiUrlService.product + "getCategories")
  }


  getProductBySubCategory(subCategoryID:number,pageNumber:number,orderBy:string,asc:boolean):Observable<Product[]>{
    return this.http.get<Product[]>(this.apiUrlService.product + "subCategory/" + subCategoryID+"/"+pageNumber
      +"/"+orderBy+"/"+asc);
  }

  getImageForProduct(filename:String):Observable<string>{
    return this.http.get<string>(this.apiUrlService.image+filename);
  }

  getProductCountByKeyword(searchparam:string):Observable<number> {

    return this.http.get<number>(this.apiUrlService.product + "query/" +
      searchparam+this.apiUrlService.countModifier)
  }

  getProductCountBySubCategory(subCategoryID:number):Observable<number> {
    return this.http.get<number>(this.apiUrlService.product + "subCategory/" + subCategoryID+
    this.apiUrlService.countModifier);
  }
}
