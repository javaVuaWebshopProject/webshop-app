import {Injectable} from '@angular/core';
import {Item} from '../../model/item';
import {ApiUrlService} from './apiurl.service';
import {CurrentUserService} from './currentUserService';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class ItemService{

  constructor(private http: HttpClient,
              private currentUserService: CurrentUserService,
              private apiUrlService: ApiUrlService) {
  }
 orderItems(items:Item[]):Observable<boolean>{
    let headers = this.currentUserService.getAuthHeader();

   return this.http.post<boolean>(this.apiUrlService.item+"orderItems",items,{headers:headers})
 }
}
