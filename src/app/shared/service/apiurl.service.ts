import {Injectable} from '@angular/core';
import {CurrentUserService} from './currentUserService';
import {HttpHeaders} from '@angular/common/http';

@Injectable()
export class  ApiUrlService{


  constructor() {
  }

  public apiRawUrl:string = "http://localhost:8080/webstoreApi/";
  public users:string = this.apiRawUrl +"users/";
  public login:string = this.apiRawUrl +"login";
  public role:string = this.users +"getRole/";
  public product:string = this.apiRawUrl +"products/";
  public order:string = this.apiRawUrl +"orders/";
  public item:string = this.apiRawUrl+"items/";
  public visit:string = this.apiRawUrl+"visit/";
  public address:string = this.apiRawUrl+"address/";
  public image:string = this.apiRawUrl+"products/image/";
  public countModifier:string ="/count";



}
