import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Rx';



import {ApiUrlService} from './apiurl.service';
import {CurrentUserService} from './currentUserService';
import {AppUser} from '../../model/appUser';




@Injectable()
export class  LoginRegisterService {

  constructor(private _http: HttpClient, private apiService: ApiUrlService,private userService:CurrentUserService) {
  }

  loginUser(username: string, password: string): Observable<any> {

    let UserLoginData: AppUser = new AppUser();
    UserLoginData.username = username;
    UserLoginData.password = password;


    return this._http.post<any>(this.apiService.login, JSON.stringify(UserLoginData), {observe: 'response'})
      .map(value => {
        return value
      })
      .catch(this.handleError)
  }


  private handleError(err: HttpErrorResponse) {
    return Observable.throw(err);
  }


  getUserRole(username: string): Observable<AppUser> {
    let headers = new HttpHeaders({
      "authorization": this.userService.getUserKey(),
      "content-type": 'application/json'
    });

    return this._http.get<AppUser>(this.apiService.role + username, {headers: headers})
      .catch(this.handleError);
  }


  registerUser(registerDetails: AppUser): Observable<string> {
    let headers = new HttpHeaders({"content-type": 'application/json'});
    return this._http.post<string>(this.apiService.users + "registerUser", JSON.stringify(registerDetails), {headers: headers});
  }


}
