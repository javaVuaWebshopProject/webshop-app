import {AbstractControl} from '@angular/forms';

export function passwordValidator(c:AbstractControl) {

  if(!c.parent || !c){return;}

  let password = c.parent.get("password");
  let confirmPassword = c.parent.get("confirmPassword")

  if(password.value === confirmPassword.value){
    return null
  }else{
    return {match: true}
  }
}
