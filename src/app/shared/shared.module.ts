import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StepperComponentComponent } from './stepper-component/stepper-component.component';
import {FormsModule} from '@angular/forms';
import {SafeImage} from './pipes/safeImage';
import { CustomModalComponent } from './custom-modal/custom-modal.component';
import {PaginatorComponent} from './paginator/paginator.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule
  ],
  declarations: [StepperComponentComponent,SafeImage, CustomModalComponent,PaginatorComponent],
  exports:[StepperComponentComponent,SafeImage,PaginatorComponent]
})
export class SharedModule { }
