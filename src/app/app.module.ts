import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import {RoutingModule} from './shared/routing/routing.module';
import {HomeModule} from './home/home.module';
import {ProductModule} from './product/product.module';
import {UserProfileModule} from './user-profile/user-profile.module';
import {ReactiveFormsModule} from '@angular/forms';
import {HttpClient, HttpClientModule, HttpHandler} from '@angular/common/http';
import {CartModule} from './cart/cart.module';
import {AdministrationModule} from './administration/administration.module';
import {ApiUrlService} from './shared/service/apiurl.service';
import {OrderService} from './shared/service/orderService';
import {ItemService} from './shared/service/itemService';
import {UsersService} from './shared/service/usersService';



import { NotFoundComponent } from './not-found/not-found.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    NotFoundComponent
  ],
  imports: [
    BrowserModule,
    RoutingModule,
    HomeModule,
    ProductModule,
    UserProfileModule,
    HttpClientModule,
    ReactiveFormsModule,
    CartModule,
    AdministrationModule
  ],
  providers: [HttpClient,ApiUrlService,OrderService,ItemService,UsersService],
  bootstrap: [AppComponent]
})
export class AppModule {


}
