# WebshopApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.7.1.

## Instalation

Clone this repo and run `npm install`  to install the required packages

## Development server

Run `ng start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
